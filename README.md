# README #

A "getting started" manual for Dockers. CIS 322, Introduction to Software Engineering, at the University of Oregon. Reference: https://docs.docker.com/engine/reference/builder/

## Description

The purpose of thie project is to implement the same "file checking" logic that was implemented in project 1 through the use of flask.

File check: If a file exists, respond with the proper file. If the file doesn't exist, respond with the corresponding error and file in the body.

  * "404.html" will display "File not found!"

  * "403.html" will display "File is forbidden!"


## Usage 

* Go to the web folder in the repository

* Build the simple flask app image using

  ```
  docker build -t UOCIS-flask-demo .
  ```
  
* Run the container using
  
  ```
  docker run -d -p 5000:5000 UOCIS-flask-demo
  ```

* To test, launch http://127.0.0.1:5000 using web browser. "UOCIS docker demo!" will be the home page.

# Contributors #
----------------

Ronny Fuentes <ronnyf@uoregon.edu>

