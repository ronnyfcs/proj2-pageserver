from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def hello():
    return "UOCIS docker demo!"

@app.route("/trivia.html")
def htmlPage():
    return render_template("trivia.html")

@app.route("/trivia.css")
def cssPage():
    return render_template("trivia.css")


@app.route("/<path:url>")
def invalid(url):
    if "//" in url or url[0] == "~" or url[:2] == "..":
        return render_template("403.html"), 403
    else:
        return render_template("404.html"), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
